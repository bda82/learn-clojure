;; Генерация случайного числа с функциями `rand` и `rand-int`.

;; Генерация числа с плавающей точкой между 0 и 1 (не включительно)
(defn random-float []
    (rand))

;; Генерация случайного целого между 0 и n (не включительно)

(defn random-int [n]
    (rand-int n))

;; (defn -main []
;;     (println (random-float))
;;     (println (random-int 10)))

;; Генерация числа с плавающей точкой между min (включительно) и max (не включительно)

(defn random-float-range [min max]
    (+ min (* (rand) (- max min))))

;; Генерация целого между min (включительно) and max (включительно)

(defn random-int-range [min max]
    (+ min (rand-int (+ 1 (- max min)))))

;; (defn -main []
;;     (println (random-float-range 1.0 2.0))
;;     (println (random-int-range 10 20)))

;; Генерация случайного булева значения (функция rand-int и проверка результата на 0.

(defn random-boolean []
  (= 0 (rand-int 2)))

;; (defn -main []
;;   (println (random-boolean)))

;; Генерация случайной строки.

(defn random-string [length]
    (apply str (repeatedly length #(char (+ (rand-int 26) 97)))))

;; (defn -main []
;;     (println (random-string 10)))

;; Генерация случайного UUID
    
(defn random-uuid []
    (str (java.util.UUID/randomUUID)))

;; (defn -main []
;;     (println (random-uuid)))

;; Генерация случайного пароля заданной длины

(defn random-password [length]
    (let [chars "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"]
        (apply str (repeatedly length #(nth chars (rand-int (count chars)))))))

;; (defn -main []
;;     (println (random-password 10)))

;; Генерация случайной последовательности целых чисел

(defn random-int-sequence [n length]
    (take length (repeatedly #(rand-int n))))

;; Генерация случайной последовательности чисел с плавающей точкой

(defn random-float-sequence [length]
    (take length (repeatedly #(rand))))

;; (defn -main []
;;     (println (random-int-sequence 10 5))
;;     (println (random-float-sequence 5)))