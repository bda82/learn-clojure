# learn-clojure

## Установка Clojure

### MacOS

Для установки Clojure на Mac, вы можете использовать менеджер пакетов Homebrew. Вот шаги, которые нужно выполнить:

Откройте терминал.

Если у вас еще нет Homebrew, установите его, выполнив следующую команду:

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

После установки Homebrew, установите Clojure, выполнив следующую команду:

```bash
brew install clojure/tools/clojure
```

После установки вы сможете запустить Clojure REPL, введя команду clj в терминале.

### Windows

Откройте командную строку (cmd) или PowerShell в режиме администратора.

Если у вас еще нет Chocolatey, установите его, выполнив следующую команду:

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

После установки Chocolatey, установите Clojure, выполнив следующую команду:

```bash
choco install clojure
```

После установки вы также сможете запустить Clojure REPL, введя команду clj в терминале.

### Linux

Для установки Clojure на Linux, вы можете использовать скрипт установки, предоставляемый официальным сайтом Clojure. Вот шаги, которые нужно выполнить:

Откройте терминал.

Скачайте скрипт установки с помощью команды curl:

```bash
curl -O https://download.clojure.org/install/linux-install-1.10.3.1040.sh
```

Сделайте скрипт исполняемым:

```bash
chmod +x linux-install-1.10.3.1040.sh
```

Запустите скрипт установки:

```bash
sudo ./linux-install-1.10.3.1040.sh
```

После установки вы сможете запустить Clojure REPL, введя команду clj в терминале.

## Установка утилиты clj-new

Для создания проектов мы будем использовать утилиту clj-new. clj-new - это библиотека Clojure, которую можно использовать для создания новых проектов Clojure. 

Сначала установим ее

### MacOS

```bash
brew install clj-new
```

### Windwos

Создайте файл deps.edn в вашем домашнем каталоге, если он еще не существует.

Добавьте следующий код в ваш файл deps.edn:

```clojure
{:aliases
 {:new {:extra-deps {seancorfield/clj-new {:mvn/version "1.1.334"}}
   :main-opts ["-m" "clj-new.create"]}}}
```

Для установки clj-new на Windows, вам сначала нужно установить Clojure и Leiningen. После этого вы можете установить clj-new следующим образом:

Теперь вы можете использовать clj-new для создания нового проекта с помощью следующей команды:

```bash
clj -A:new app myname/myapp
```

### Linux 

Для Linux установка происходит точно так же, как и для Windows

## Установка утилиты Leiningen

Leiningen - это инструмент для автоматизации проектов на языке программирования Clojure. Он обрабатывает установку зависимостей, компиляцию исходного кода, запуск тестов и сборку готовых к использованию пакетов.

Leiningen предоставляет простой и удобный способ управления проектами Clojure, позволяя разработчикам сосредоточиться на написании кода, а не на ручной настройке инфраструктуры проекта.

Leiningen предоставляет ряд функций для управления проектами на Clojure, включая:

1. Управление зависимостями: Leiningen автоматически загружает и управляет библиотеками, необходимыми для вашего проекта. Вы просто указываете, какие библиотеки вам нужны, и Leiningen сделает остальное.
2. Сборка проекта: Leiningen может компилировать ваш исходный код и собирать его в исполняемый JAR-файл.
3. Запуск тестов: Leiningen может автоматически запускать ваши тесты и сообщать о результатах.
4. Создание новых проектов: Leiningen может генерировать шаблоны проектов, чтобы помочь вам быстро начать новый проект.
5. Запуск REPL: Leiningen может запускать Clojure REPL, что упрощает интерактивную разработку и тестирование.
6. Управление скриптами: Leiningen позволяет определить и запускать пользовательские скрипты, которые могут выполнять различные задачи, такие как генерация документации, выполнение миграций базы данных и т.д.

### MacOS 

```bash
brew install leiningen
```

### Windows

Скачайте скрипт установки Leiningen из официального репозитория на GitHub. Вы можете сделать это, перейдя по следующей ссылке в вашем браузере: https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein.bat

Сохраните файл lein.bat в каталоге, который находится в вашем PATH. Обычно это C:\Windows\System32.

Откройте командную строку (cmd) и введите lein. Это запустит процесс установки Leiningen.

### Linux

Откройте терминал.

Скачайте скрипт установки Leiningen с помощью команды wget:

```bash
wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
```

Сделайте скрипт исполняемым:

```bash
chmod +x lein
```

Переместите скрипт в каталог, который находится в вашем PATH. Обычно это /usr/local/bin:

```bash
sudo mv lein /usr/local/bin
```

После установки вы сможете использовать Leiningen, введя команду lein в терминале.

## Создание и запуск проекта на Clojure

Выполним команду для создания нового проекта

```bash
clj -X:new :template app :name my-project
```

Для нас будет создана директория my-project со следующей структурой

```bash
my-project/
├── doc/
├── resources/
├── src/
│   └── my_project/
│       └── core.clj
├── test/
│   └── my_project/
│       └── core_test.clj
└── project.clj
```

Основной файл проекта будет называться core.clj. Определим его содержимое следующим образом

```clojure
(ns my-project.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
```

Здесь определяется пространство имен my-project.core и добавляетс основная функция, которая печатает Hello, World в консоли

Далее можно открыть файл project.clj в редакторе и добавить в него описание проекта

```clojure
(defproject my-project "0.1.0-SNAPSHOT"
  :description "My first Clojure project."
  :url "https://github.com/<username>/my-project"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]]
  :main my-project.core)
```

Это метаданные проекта, включая основные зависиомости и основную функцию запуска

Далее можно открыть терминал, перейти в директорию проекта и запустить Clojure REPL следующей командой

```bash
clj
```

В REPL следующей командой можно скомпилировать и запустить основную функцию

```clojure
(clojure.main/run my-project.core)
```

## Дополнительные возможности clj-new

clj-new предоставляет несколько вариантов использования для создания новых проектов Clojure:

Создание нового приложения: Вы можете использовать clj-new для создания нового приложения Clojure. Например, следующая команда создаст новое приложение с именем my-app:

```bash
clj -X:new :template app :name myname/my-app
```

Создание новой библиотеки: Вы можете использовать clj-new для создания новой библиотеки Clojure. Например, следующая команда создаст новую библиотеку с именем my-lib:

```bash
clj -X:new :template lib :name myname/my-lib
```

Создание нового проекта на основе шаблона: clj-new поддерживает шаблоны, которые позволяют вам быстро создавать проекты с определенной структурой. Например, следующая команда создаст новый проект на основе шаблона reagent:

```bash
clj -X:new :template reagent :name myname/my-reagent-app
```

Создание нового плагина для Leiningen или Boot: clj-new также может создавать плагины для Leiningen или Boot. Например, следующая команда создаст новый плагин для Leiningen:

```bash
clj -X:new :template lein-plugin :name myname/my-lein-plugin
```

## Средства разработки на Clojure

### Visual Studio Conde

Плагин Calva: Clojure & ClojureScript Interactive Programming
https://marketplace.visualstudio.com/items?itemName=betterthantomorrow.calva