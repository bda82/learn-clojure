# Map (hash)

В Clojure, map (или hash-map) - это тип коллекции, который хранит данные в форме пар ключ-значение. Каждый ключ в map уникален и связан с определенным значением.

Вот пример создания map в Clojure:
```clojure
(def my-map {:key1 "value1" :key2 "value2" :key3 "value3"})
```

В этом примере `:key1`, `:key2` и `:key3` - это ключи, а `"value1"`, `"value2"` и `"value3`" - это соответствующие значения.

Вы можете получить доступ к значению по ключу с помощью функции `get`:
```clojure
(get my-map :key1) ; => "value1"
```

Или вы можете использовать ключ как функцию:
```clojure
(:key1 my-map) ; => "value1"
```

Вы также можете добавить новые пары ключ-значение или обновить существующие значения с помощью функции `assoc`:
```clojure
(assoc my-map :key4 "value4") ; => {:key1 "value1", :key2 "value2", :key3 "value3", :key4 "value4"}
```

И удалить ключи с помощью функции `dissoc`:
```clojure
(dissoc my-map :key1) ; => {:key2 "value2", :key3 "value3"}
```

Как и все коллекции в Clojure, map является неизменяемым, что означает, что функции assoc и dissoc возвращают новый map, а не изменяют существующий.

## Как объединить два map

В Clojure, вы можете объединить два map с помощью функции `merge`. Эта функция принимает два или более `map` и возвращает новый `map`, который содержит все пары ключ-значение из всех переданных `map`. Если несколько `map` содержат одинаковые ключи, то значение будет взято из последнего `map`.

Вот пример:
```clojure
(def map1 {:a 1, :b 2})
(def map2 {:b 3, :c 4})

(merge map1 map2) ; => {:a 1, :b 3, :c 4}
```

В этом примере, значение для ключа `:b` в результирующем `map` равно 3, потому что `map2` был последним, и его значение для `:b` перезаписывает значение из `map1`.

## Как определить наличие ключа в map

В Clojure, вы можете проверить наличие ключа в `map` с помощью функции `contains?`. Эта функция принимает map и ключ, и возвращает `true`, если ключ присутствует в `map`, и `false` в противном случае.

Вот пример:
```clojure
(def my-map {:a 1, :b 2, :c 3})

(contains? my-map :a) ; => true
(contains? my-map :d) ; => false
```

В этом примере, `(contains? my-map :a)` возвращает `true`, потому что ключ `:a` присутствует в `my-map`, а `(contains? my-map :d)` возвращает `false`, потому что ключ `:d` отсутствует в `my-map`.

## Как получить список ключей из map

В Clojure, вы можете получить все ключи из map с помощью функции keys. Эта функция принимает map и возвращает последовательность, содержащую все ключи этого map.

Вот пример:
```clojure
(def my-map {:a 1, :b 2, :c 3})

(keys my-map) ; => (:a :b :c)
```

## Как получить все значения из map

В Clojure, вы можете получить все значения из map с помощью функции vals. Эта функция принимает map и возвращает последовательность, содержащую все значения этого map.

Вот пример:
```clojure
(def my-map {:a 1, :b 2, :c 3})

(vals my-map) ; => (1 2 3)
```

## Как проверить, пустой ли map

В Clojure, вы можете проверить, пустой ли map, с помощью функции empty?. Эта функция принимает коллекцию и возвращает true, если коллекция пуста, и false в противном случае.

Вот пример:

```clojure
(def my-map {:a 1, :b 2, :c 3})
(def empty-map {})

(empty? my-map) ; => false
(empty? empty-map) ; => true
```

## Как получить количество элементов в map

В Clojure, вы можете получить количество элементов в map с помощью функции count. Эта функция принимает коллекцию и возвращает количество элементов в этой коллекции.

Вот пример:

```clojure
(def my-map {:a 1, :b 2, :c 3})

(count my-map) ; => 3
```

## Как получить все пары ключ значение из map

В Clojure, вы можете получить все пары ключ-значение из map с помощью функции seq или просто итерировать по map, так как map в Clojure является последовательностью пар ключ-значение.

Вот пример:

```clojure
(def my-map {:a 1, :b 2, :c 3})

(seq my-map) ; => ([:a 1] [:b 2] [:c 3])
```

В этом примере, (seq my-map) возвращает последовательность, содержащую все пары ключ-значение из my-map в виде векторов.

Вы также можете итерировать по map с помощью цикла for:

```clojure
(for [[k v] my-map]
    (println "Key:" k "Value:" v))
```

Этот код будет печатать каждую пару ключ-значение из my-map.


## Как можно изменить значение ключа в map без создания нового map

В Clojure, все коллекции являются неизменяемыми, включая map. Это означает, что вы не можете изменить значение ключа в map без создания нового map.

Функция assoc возвращает новый map с измененным значением ключа, но исходный map остается неизменным. Это является основой функционального программирования и позволяет избегать многих проблем, связанных с изменяемым состоянием.

Если вам нужно часто изменять значения в map, возможно, вам стоит рассмотреть использование атомов (atom), которые предоставляют безопасный способ управления изменяемым состоянием. Вы можете использовать swap! с assoc для изменения значения в атоме, содержащем map:

```clojure
(def my-map (atom {:a 1, :b 2, :c 3}))

(swap! my-map assoc :b 20)

@my-map ; => {:a 1, :b 20, :c 3}
```

В этом примере, my-map является атомом, содержащим map. swap! применяет функцию assoc к текущему значению атома, и результат становится новым значением атома. @my-map используется для получения текущего значения атома.

## Как можно преобразовать map в вектор

В Clojure, вы можете преобразовать map в вектор с помощью функции vec. Однако, это преобразует map в вектор пар ключ-значение. Если вы хотите получить вектор только ключей или только значений, вы можете использовать функции keys и vals соответственно перед преобразованием в вектор.

Вот пример:

```clojure
(def my-map {:a 1, :b 2, :c 3})

(vec my-map) ; => [[:a 1] [:b 2] [:c 3]]
(vec (keys my-map)) ; => [:a :b :c]
(vec (vals my-map)) ; => [1 2 3]
```

В этом примере, (vec my-map) возвращает вектор пар ключ-значение, (vec (keys my-map)) возвращает вектор ключей, а (vec (vals my-map)) возвращает вектор значений.

## Как можно преобразовать вектор в map

В Clojure, вы можете преобразовать вектор в map с помощью функции into. Однако, вектор должен быть вектором пар ключ-значение.

Вот пример:

```clojure
(def my-vector [[:a 1] [:b 2] [:c 3]])

(into {} my-vector) ; => {:a 1, :b 2, :c 3}
```

В этом примере, (into {} my-vector) возвращает map, созданный из пар ключ-значение в my-vector.

Если у вас есть вектор ключей и вектор значений, вы можете использовать функцию zipmap для создания map:

```clojure
(def keys [:a :b :c])
(def values [1 2 3])

(zipmap keys values) ; => {:a 1, :b 2, :c 3}
```

В этом примере, (zipmap keys values) возвращает map, где ключи и значения взяты из векторов keys и values соответственно.

## Как преобразовать map в список

В Clojure, вы можете преобразовать map в список с помощью функции seq. Это преобразует map в список пар ключ-значение. Если вы хотите получить список только ключей или только значений, вы можете использовать функции keys и vals соответственно перед преобразованием в список.

Вот пример:

```clojure
(def my-map {:a 1, :b 2, :c 3})

(seq my-map) ; => ([:a 1] [:b 2] [:c 3])
(seq (keys my-map)) ; => (:a :b :c)
(seq (vals my-map)) ; => (1 2 3)
```

В этом примере, (seq my-map) возвращает список пар ключ-значение, (seq (keys my-map)) возвращает список ключей, а (seq (vals my-map)) возвращает список значений.

## Как преобразовать список в map

В Clojure, вы можете преобразовать список в map с помощью функции into. Однако, список должен быть списком пар ключ-значение.

Вот пример:

```clojure
(def my-list '([:a 1] [:b 2] [:c 3]))

(into {} my-list) ; => {:a 1, :b 2, :c 3}
```

В этом примере, (into {} my-list) возвращает map, созданный из пар ключ-значение в my-list.

Если у вас есть список ключей и список значений, вы можете использовать функцию zipmap для создания map:

```clojure
(def keys '(:a :b :c))
(def values '(1 2 3))

(zipmap keys values) ; => {:a 1, :b 2, :c 3}
```

В этом примере, (zipmap keys values) возвращает map, где ключи и значения взяты из списков keys и values соответственно.

# Директивы

Директива :as позволяет создавать копии для хешей, например, для того, чтобы при преобразованиях сохранить исходный хэш.

```clojure
(def m {:x 11 :y 22 :z 33})
(let [{r1 :x r2 :y :as m1} m] (assoc m1 :sum (+ r1 r2))) → {:x 11, :y 22, :z 33, :sum 33}
```

Здесь m1 является рабочей копией для m.

Директива :or позволяет задать значение по умолчанию, если нужный элемент не будет найден:
```clojure
(def m {:a 5 :b 7 :c 1})
(let [{k :u x :a :or {k 50}} m] (+ k x)) → 55
```

Для инициации локальной переменной k предпринимается попытка извлечь элемент с ключом u. Поскольку такой элемент отсутствует, принимается значение по умолчанию — 50.

```clojure
(def m {:a 5 :u 7 :c 1})
let [{k :u x :a :or {k 50}} m] (+ k x)) → 12
```

Теперь такой элемент есть.

Есть ещё один вариант директивы or при котором он способен воспринимать значения true или false (в условных выражениях nil тождественен false).
Объявим переменную k и инициируем её каким-либо значением: (def k 7)
Теперь используем k в форме let с директивой or:

```clojure
(let [k (or k 50)] k) → 7 - значение k не равно nil, значение по умолчанию отвергается.
(def k nil)
(let [k (or k 50)] k) → 50 - теперь принято значение по умолчанию. 
```

Этим вариантом тоже можно воспользоваться для извлечения из хэша с заданием значения по умолчанию:
```clojure
(let [{k :u x :a} m k(or k 50)] (+ k x))
```

Если ключа :u нет, то переменная k получит значение nil и произойдёт замена по умолчанию.

