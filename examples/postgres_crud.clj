;; Для начала необходимо добавить бибилотеку clj-kondo в зависимости проекта путем модификации project.clj
[clj-kondo "0.0.53"]

;; Далее необходимо добавить библиотеку clj-kondo в исходный код
(require '[clj-kondo.core :as kondo])

;; Теперь определим функцию для соединения с базой данных PostgreSQL
(defn open-db []
  (kondo/connect {:dbtype :postgres
                  :host "localhost"
                  :port 5432
                  :dbname "my-database"
                  :user "my-user"
                  :password "my-password"}))

;; Теперь определим функции CRUD для таблицы пользователей
(defn create-user [db user]
  (kondo/insert! db :users user))

;; Получение пользователя по его ID
(defn get-user [db id]
  (first (and (seq (kondo/query db ["SELECT * FROM users WHERE id = ?" id])))))

;; Получение списка пользователей
(defn list-users [db]
  (kondo/query db ["SELECT * FROM users"]))

;; Обновление пользователя
(defn update-user [db user]
  (kondo/update! db :users {:name (:name user) :email (:email user)} [:id (:id user)]))

;; Удаление пользователя
(defn delete-user [db id]
  (kondo/delete! db :users [:id id]))

;; Далее определим основную функцию, которая открывает соединение с базой данных, CRUD-операции, а потом закрывает соединение
(defn -main []
  (let [db (open-db)]
    (create-user db {:name "John Doe" :email "john.doe@example.com"})
    (println (get-user db 1))
    (update-user db {:name "Jane Doe" :email "jane.doe@example.com"})
    (println (get-user db 1))
    (delete-user db 1)
    (println (list-users db))
    (kondo/close db)))

