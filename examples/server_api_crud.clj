(ns my-api-server
  (:require [cheshire.core :as json]
            [compojure.core :refer [GET POST PUT DELETE]
                                  [not-found response]]
            [compojure.route :as route]
            [ring.middleware.cors :as cors]
            [ring.middleware.defaults :as defaults]
            [ring.util.response :as response]
            [ring.util.sequence :as seq]
            [clojure.string :as str]
            [honey.sql "0.7.0"]
            [honey.core :as sql]
            [honey.db.pool :as pool]))

(defn- user-exists? [id]
  (sql/with-db [conn (pool/get-conn)]
    (sql/query conn ["(select * from users where id = ?)" id])))

(defn- create-user [user]
  (sql/with-db [conn (pool/get-conn)]
    (sql/insert! conn {:name (:name user)
                     :email (:email user)})
    (first (sql/query conn ["(select last_insert_id())"]))))

(defn- get-user [id]
  (sql/with-db [conn (pool/get-conn)]
    (first (sql/query conn ["(select * from users where id = ?)" id])))

(defn- update-user [user]
  (sql/with-db [conn (pool/get-conn)]
    (sql/update! conn {:name (:name user)
                     :email (:email user)}
                     [:id (:id user)])))

(defn- delete-user [id]
  (sql/with-db [conn (pool/get-conn)]
    (sql/delete! conn :users [:id id])))

(defn list-users []
  (sql/with-db [conn (pool/get-conn)]
    (sql/query conn ["(select * from users)"])))

(defroutes app-routes
  (POST "/users" {:keys [name email] :as req}
    (let [id (create-user {:name name :email email})]
      (response/response (json/generate-string {:status :created :id id}) {:status 201})))
  (GET "/users" []
    (response/response (json/generate-string {:users (list-users)}))
  (GET "/users/:id" [id]
    (let [user (get-user id)]
      (if (seq user)
        (response/response (json/generate-string user))
        (not-found "User not found")))
  (PUT "/users/:id" {:keys [name email] :as req}
    (let [id (Integer. (str (get-in req [:params :id]))]
      (if (user-exists? id)
        (do
          (update-user {:id id :name name :email email})
          (response/response (json/generate-string {:status :updated}))))
        (not-found "User not found")))
  (DELETE "/users/:id" [id]
    (let [id (Integer. (str (get-in req [:params :id]))]
      (if (user-exists? id)
        (do
          (delete-user id)
          (response/response (json/generate-string {:status :deleted})))
        (not-found "User not found")))
  (route/not-found "Not Found"))

(def app
  (-> app-routes
      (cors/wrap-cors)
      (defaults/wrap-defaults)))

(defn -main []
  (sql/start!)
  (jetty/run-jetty app {:port 3000}))

