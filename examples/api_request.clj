(ns my-api-client
  (:require [clj-http.client :as http]))

(defn get-api [url]
  (http/get url))

(defn -main []
  (let [response (get-api "https://jsonplaceholder.typicode.com/posts/1")]
    (println (:body response))))

;; get-api - функция, которая делает GET-запрос к API.
;; url - адрес API.
;; response - ответ от API.
;; -main - главная функция, которая вызывает get-api и печатает ответ от API.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-api-client/-main)

;; Важно отметить, что в этом примере используется библиотека clj-http.client для делания HTTP-запросов. Функция http/get используется для делания GET-запроса к API.
;; Важно также отметить, что перед использованием библиотеки clj-http.client вам нужно будет добавить зависимость на нее в файл project.clj:
[clj-http "4.11.0"]

;; После этого, вы должны выполнить команду lein deps для установки зависимостей.
;; Полную документацию и инструкции по установке библиотеки clj-http вы можете найти по ссылке: https://github.com/dakrone/clj-http