;; Для начала необходимо добавить пакет clj-sqlite в зависимости проекта. Это можно сделать припомощи project.clj файла
[clj-sqlite "1.2.1"]

;; Затем необходимо добавить библиотеку clj-sqlite в исходный код
(require '[clj-sqlite.core :as sqlite])

;; Далее необходимо определить функцию для создания соединения к SQLite базе данных
(defn open-db []
  (sqlite/open "my-database.db"))

;; Теперь можно определить функции для CRUD-операций

;; Создание пользователя
(defn create-user [db user]
  (sqlite/insert! db :users user))

;; Получение пользователя по его ID
(defn get-user [db id]
  (first (and (seq (sqlite/query db ["SELECT * FROM users WHERE id = ?" id])))))

;; Получение списка пользоваленей
(defn list-users [db]
  (sqlite/query db ["SELECT * FROM users"]))

;;  Обновление пользователя
(defn update-user [db user]
  (sqlite/update! db :users {:name (:name user) :email (:email user)} [:id (:id user)]))

;; Удаление пользователя
(defn delete-user [db id]
  (sqlite/delete! db :users [:id id]))

;; Наконец, можно сформировать основную функцию, которая создаст соединение с БД, определить CRUD-операции и закроет содеинение
(defn -main []
  (let [db (open-db)]
    (create-user db {:name "John Doe" :email "john.doe@example.com"})
    (println (get-user db 1))
    (update-user db {:name "Jane Doe" :email "jane.doe@example.com"})
    (println (get-user db 1))
    (delete-user db 1)
    (println (list-users db))
    (sqlite/close db)))