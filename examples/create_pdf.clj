(ns my-pdf-generator
  (:import (java.io FileOutputStream)
            (org.apache.pdfbox.pdmodel.PDDocument)
            (org.apache.pdfbox.pdmodel.PDPage)
            (org.apache.pdfbox.pdmodel.PDPageContentStream)
            (org.apache.pdfbox.pdmodel.common.PDRectangle)
            (org.apache.pdfbox.pdmodel.font.PDType1Font)
            (org.apache.pdfbox.pdmodel.graphics.state.PDGraphicsState)
            (java.awt.Color)))

(defn create-pdf [file-path]
  (let [output-stream (FileOutputStream. (File. file-path))
        document (PDDocument.)
        page (PDPage. (PDRectangle. A4))
        content-stream (PDPageContentStream. document page :append true)]
    (.setFont content-stream (PDType1Font. DEFAULT_FONT_NAME) 12)
    (.setNonStrokingColor content-stream (Color. 0 0 0))
    (.beginText content-stream)
    (.newLineAtOffset content-stream 50 700)
    (.showText content-stream "Hello, PDF!")
    (.endText content-stream)
    (.close content-stream)
    (.addPage document page)
    (.save document output-stream)
    (.close document)
    (.close output-stream)))

(defn -main []
  (create-pdf "path/to/pdf/file.pdf"))

;; create-pdf - функция, которая создает PDF-файл.
;; file-path - путь к PDF-файлу.
;; output-stream - поток вывода для PDF-файла.
;; document - объект PDDocument, который представляет PDF-файл.
;; page - объект PDPage, который представляет страницу PDF-файла.
;; content-stream - объект PDPageContentStream, который используется для рисования на странице PDF-файла.
;; PDRectangle - класс, который представляет размеры страницы PDF-файла.
;; PDType1Font - класс, который представляет шрифт PDF-файла.
;; PDGraphicsState - класс, который представляет состояние графики PDF-файла.
;; Color - класс, который представляет цвет PDF-файла.
;; DEFAULT_FONT_NAME - константа, которая представляет имя шрифта по умолчанию.
;; -main - главная функция, которая вызывает create-pdf и создает PDF-файл.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-pdf-generator/-main)

;; Важно отметить, что в этом примере используется библиотека PDFBox из Apache. Эта библиотека предоставляет функциональность для работы с PDF-файлами. Функция FileOutputStream используется для создания потока вывода для PDF-файла. Функция PDDocument используется для создания объекта, представляющего PDF-файл. Функция PDPage используется для создания объекта, представляющего страницу PDF-файла. Функция PDPageContentStream используется для рисования на странице PDF-файла.
;; Важно также отметить, что перед использованием библиотеки PDFBox вам нужно будет добавить зависимость на нее в файл project.clj:
