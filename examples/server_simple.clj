(ns my-web-server
  (:require [ring.adapter.jetty :as jetty]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]))

(defn handler [request]
  {:status 200
   :headers {"Content-Type" "text/plain"}
   :body "Hello, Clojure!"})

(defn -main []
  (jetty/run-jetty (wrap-defaults handler site-defaults) {:port 3000}))