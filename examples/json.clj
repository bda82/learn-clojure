(ns my-json-generator
  (:require [cheshire.core :as json]))

(defn generate-json []
  (json/generate-string {:name "John" :age 30 :city "New York"}))

(defn -main []
  (let [json-string (generate-json)]
    (println json-string)))

;; generate-json - функция, которая формирует JSON.
;; json-string - ответ в виде JSON.
;; -main - главная функция, которая вызывает generate-json и печатает JSON в консоли.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-json-generator/-main)

;; Важно отметить, что в этом примере используется библиотека cheshire.core для формирования JSON. Функция json/generate-string используется для формирования JSON в виде строки.
;; Важно также отметить, что перед использованием библиотеки cheshire.core вам нужно будет добавить зависимость на нее в файл project.clj:
[cheshire "5.10.0"]

;; После этого, вы должны выполнить команду lein deps для установки зависимостей.
;; Полную документацию и инструкции по установке библиотеки cheshire вы можете найти по ссылке: https://github.com/dakrone/cheshire
