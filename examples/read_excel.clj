(ns my-excel-reader
  (:import (java.io InputStream)
            (org.apache.poi.ss.usermodel.Cell)
            (org.apache.poi.ss.usermodel.Row)
            (org.apache.poi.ss.usermodel.Sheet)
            (org.apache.poi.ss.usermodel.Workbook)
            (org.apache.poi.xssf.usermodel.XSSFWorkbook)))

(defn read-excel [file-path]
  (let [input-stream (InputStream. (FileInputStream. (File. file-path)))]
    (let [workbook (XSSFWorkbook. input-stream)]
      (let [sheet (.getSheetAt workbook 0)]
        (map (fn [row]
               (map (fn [cell]
                      (.getStringCellValue cell))
                    (.getRow row)))
             (.getRows sheet))))))

(defn -main []
  (let [data (read-excel "path/to/excel/file.xlsx")]
    (println data)))

;; read-excel - функция, которая читает данные из Excel-файла.
;; file-path - путь к Excel-файлу.
;; input-stream - поток ввода для Excel-файла.
;; workbook - объект Workbook, который представляет Excel-файл.
;; sheet - объект Sheet, который представляет лист в Excel-файле.
;; row - объект Row, который представляет строку в Excel-файле.
;; cell - объект Cell, который представляет ячейку в Excel-файле.
;; -main - главная функция, которая вызывает read-excel и печатает данные из Excel-файла.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-excel-reader/-main)

;; Важно отметить, что в этом примере используется библиотека POI из Apache. Эта библиотека предоставляет функциональность для работы с Excel-файлами. Функция InputStream используется для создания потока ввода для Excel-файла. Функция XSSFWorkbook используется для создания объекта, представляющего Excel-файл. Функция Sheet используется для получения объекта, представляющего лист в Excel-файле. Функция Row используется для получения объекта, представляющего строку в Excel-файле. Функция Cell используется для получения объекта, представляющего ячейку в Excel-файле. Функция .getStringCellValue используется для получения значения ячейки в виде строки.
;; Важно также отметить, что перед использованием библиотеки POI вам нужно будет добавить зависимость на нее в файл project.clj:
[org.apache.poi/poi-ooxml "5.0.0"]

;; После этого, вы должны выполнить команду lein deps для установки зависимостей.
;; Полную документацию и инструкции по установке библиотеки POI вы можете найти по ссылке: https://poi.apache.org/documentation.html



