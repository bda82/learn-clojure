;; добавим пакет clj-http в зависимости проекта
[clj-http "4.3.0"]

;; добавим пакет в исходный код
(require '[clj-http.client :as http])

;; определим функцию для POST запроса с JSON телом
(defn make-post-request [url json-body]
  (let [response (http/post url {:content-type "application/json" :body (json/generate-string json-body)})]
    (if (= (:status response) 200)
      (println "Request successful!")
      (println "Request failed with status code:" (:status response)))))

;; данная функция принимает URL и map, которая представляет собой реализацию JSON тела как аргументю
;; Функция использует http/post из модуля clj/http для формирования POST  запроса к указанному URL'у с определенной нагрузкой. Заголовок :content-type устанавливается в значение "application/json" для обозначения типа тела запроса в JSON формате.
;; Функция также проверяет код статуса ответа и печатает сообщение об успешном или неудачном запросе.

;; Можно использовать созданную функцию следующим образом
(make-post-request "https://example.com/api" {:name "John Doe" :email "johndoe@example.com"})

;; В данном примере мы формируем POST запрос с телом с ключами :name и :email.
;; Функция json/generate-string используется для преобразования Clojure map в валидную JSON строку.