(ns my-word-generator
  (:import (java.io FileOutputStream)
            (org.apache.poi.xwpf.usermodel.XWPFDocument)
            (org.apache.poi.xwpf.usermodel.XWPFParagraph)
            (org.apache.poi.xwpf.usermodel.XWPFRun)))

(defn create-word [file-path]
  (let [output-stream (FileOutputStream. (File. file-path))
        document (XWPFDocument.)]
    (let [paragraph (XWPFParagraph. (XWPFDocument.)]
      (let [run (.createRun paragraph)]
        (.setText run "Hello, Word!")
        (.createParagraph document paragraph)))
    (.write document output-stream)
    (.close output-stream)
    (.close document)))

(defn -main []
  (create-word "path/to/word/file.docx"))

;; create-word - функция, которая создает Word-файл.
;; file-path - путь к Word-файлу.
;; output-stream - поток вывода для Word-файла.
;; document - объект XWPFDocument, который представляет Word-файл.
;; paragraph - объект XWPFParagraph, который представляет абзац в Word-файле.
;; run - объект XWPFRun, который используется для записи текста в абзац Word-файла.
;; -main - главная функция, которая вызывает create-word и создает Word-файл.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-word-generator/-main)

;; Важно отметить, что в этом примере используется библиотека POI из Apache. Эта библиотека предоставляет функциональность для работы с Word-файлами. Функция FileOutputStream используется для создания потока вывода для Word-файла. Функция XWPFDocument используется для создания объекта, представляющего Word-файл. Функция XWPFParagraph используется для создания объекта, представляющего абзац в Word-файле. Функция XWPFRun используется для записи текста в абзац Word-файла.
;; Важно также отметить, что перед использованием библиотеки POI вам нужно будет добавить зависимость на нее в файл project.clj:
[org.apache.poi/poi-ooxml "5.0.0"]

;; После этого, вы должны выполнить команду lein deps для установки зависимостей.
;; Полную документацию и инструкции по установке библиотеки POI вы можете найти по ссылке: https://poi.apache.org/documentation.html