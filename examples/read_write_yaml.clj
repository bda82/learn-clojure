;; Для чтения и записи YAML-файлов в Clojure вы можете использовать библиотеку clj-yaml. Вот пример:

(ns my-namespace.core
    (:require [clojure.java.io :as io]
                        [clj-yaml.core :as yaml]))

(defn read-yaml [filename]
    (with-open [rdr (io/reader filename)]
        (yaml/parse-string (slurp rdr))))

(defn write-yaml [filename data]
    (spit filename (yaml/generate-string data)))

(defn -main []
    ;; Write YAML to a file
    (write-yaml "output.yaml" {:foo "bar" :baz [1, 2, 3]})

    ;; Read YAML from a file
    (println (read-yaml "output.yaml")))

;; В этом примере функция read-yaml читает YAML из файла с именем filename и возвращает его как структуру данных Clojure. Функция write-yaml записывает структуру данных data в файл с именем filename в формате YAML. Функция -main вызывает write-yaml, чтобы записать некоторые данные в файл "output.yaml", а затем вызывает read-yaml, чтобы прочитать эти данные обратно.