(ns my-pdf-reader
  (:import (java.io FileInputStream)
            (org.apache.pdfbox.pdmodel PDDocument)
            (org.apache.pdfbox.util PDFTextStripper)))

(defn read-pdf [file-path]
  (let [input-stream (FileInputStream. (File. file-path))
        document (PDDocument. input-stream)
        text-stripper (PDFTextStripper.)]
    (.setSortByPosition text-stripper true)
    (.setStartPage text-stripper 0)
    (.setEndPage text-stripper (.getNumberOfPages document))
    (str (.getText text-stripper))
    (.close input-stream)
    (.close document)))

(defn -main []
  (println "Content of PDF file:")
  (println (read-pdf "path/to/pdf/file.pdf")))

;; read-pdf - функция, которая читает содержимое PDF-файла и возвращает его в виде строки.
;; file-path - путь к PDF-файлу.
;; input-stream - поток ввода для PDF-файла.
;; document - объект PDDocument, который представляет PDF-файл.
;; text-stripper - объект PDFTextStripper, который используется для извлечения текста из PDF-файла.
;; -main - главная функция, которая вызывает read-pdf и выводит результаты в консоль.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-pdf-reader/-main)

;; Важно отметить, что в этом примере используется библиотека PDFBox из Apache. Эта библиотека предоставляет функциональность для работы с PDF-файлами. Функция FileInputStream используется для создания потока ввода для PDF-файла. Функция PDDocument используется для создания объекта, представляющего PDF-файл. Функция PDFTextStripper используется для извлечения текста из PDF-файла.
;; Важно также отметить, что перед использованием библиотеки PDFBox вам нужно будет добавить зависимость на нее в файл project.clj:
[org.apache.pdfbox/pdfbox "2.0.26"]

;; После этого, вы должны выполнить команду lein deps для установки зависимостей.
;; Полную документацию и инструкции по установке библиотеки PDFBox вы можете найти по ссылке: https://pdfbox.apache.org/docs/2.0.26/javadocs/index.html