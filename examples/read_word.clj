(ns my-doc-reader
  (:import (java.io FileInputStream)
            (org.apache.poi.xwpf.extractor.XWPFWordExtractor)
            (org.apache.poi.hwpf.HWPFDocument)
            (org.apache.poi.hwpf.extractor.WordExtractor)))

(defn read-doc [file-path]
  (let [input-stream (FileInputStream. (File. file-path))]
    (if (.is (.getExtension file-path) "docx")
      (do
        (let [document (.getDocument (XWPFWordExtractor. input-stream))]
          (.getText document)))
      (do
        (let [document (HWPFDocument. input-stream)]
          (WordExtractor. document))))))

(defn -main []
  (println "Content of DOC/DOCX file:")
  (println (read-doc "path/to/doc/file.docx")))

;; read-doc - функция, которая читает содержимое DOC или DOCX-файла и возвращает его в виде строки.
;; file-path - путь к DOC или DOCX-файлу.
;; input-stream - поток ввода для DOC или DOCX-файла.
;; document - объект HWPFDocument или XWPFDocument, который представляет DOC или DOCX-файл соответственно.
;; WordExtractor или XWPFWordExtractor - объект, который используется для извлечения текста из DOC или DOCX-файла соответственно.
;; -main - главная функция, которая вызывает read-doc и выводит результаты в консоль.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-doc-reader/-main)

;; Важно отметить, что в этом примере используется библиотека POI из Apache. Эта библиотека предоставляет функциональность для работы с DOC и DOCX-файлами. Функция FileInputStream используется для создания потока ввода для DOC или DOCX-файла. Функция HWPFDocument или XWPFDocument используется для создания объекта, представляющего DOC или DOCX-файл соответственно. Функции WordExtractor или XWPFWordExtractor используются для извлечения текста из DOC или DOCX-файла соответственно.
;; Важно также отметить, что перед использованием библиотеки POI вам нужно будет добавить зависимость на нее в файл project.clj:
[org.apache.poi/poi-ooxml "5.0.0"]

;; После этого, вы должны выполнить команду lein deps для установки зависимостей.
;; Полную документацию и инструкции по установке библиотеки POI вы можете найти по ссылке: https://poi.apache.org/documentation.html