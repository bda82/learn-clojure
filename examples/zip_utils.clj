(ns my-zip-utils
  (:import (java.util.zip ZipEntry ZipOutputStream ZipFile)
            (java.io InputStream OutputStream FileInputStream FileOutputStream)))

(defn zip-files [files zip-file]
  (let [output-stream (FileOutputStream. zip-file)
        zip-output-stream (ZipOutputStream. output-stream)]
    (doseq [file files]
      (let [entry (ZipEntry. (.getName file))]
        (.putNextEntry zip-output-stream entry)
        (let [input-stream (FileInputStream. file)]
          (do
            (copy input-stream zip-output-stream)
            (.close input-stream))
          (.closeEntry zip-output-stream)))
      (.close zip-output-stream)
      (.close output-stream)))

(defn unzip-file [zip-file dest-dir]
  (let [zip-file (ZipFile. zip-file)]
    (doseq [entry (.entries zip-file)]
      (let [name (.getName entry)
            dest-file (File. (str dest-dir "/" name))]
        (when (not (.isDirectory name))
          (let [input-stream (.getInputStream entry)]
            (let [output-stream (FileOutputStream. dest-file)]
              (copy input-stream output-stream)
              (.close input-stream)
              (.close output-stream))))))))

(defn -main []
  (let [files ["file1.txt" "file2.txt"]
        zip-file "files.zip"
        dest-dir "destination"]
    (println "Zipping files...")
    (zip-files files zip-file)
    (println "Unzipping files...")
    (unzip-file zip-file dest-dir)))

;; zip-files - функция, которая запаковывает файлы в формате ZIP.
;; files - список файлов, которые нужно запаковать.
;; zip-file - путь к файлу ZIP.
;; zip-output-stream - поток вывода для файла ZIP.
;; entry - объект ZipEntry, который представляет файл в архиве ZIP.
;; input-stream - поток ввода для файла.
;; output-stream - поток вывода для файла ZIP.
;; unzip-file - функция, которая распаковывает файл ZIP.
;; zip-file - путь к файлу ZIP.
;; dest-dir - каталог, в который нужно распаковать файл ZIP.
;; ZipFile - класс, который представляет файл ZIP.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-zip-utils/-main)

;; Вы увидите, что файлы были запакованы в файл files.zip и затем распакованы в каталог destination.
;; Важно отметить, что в этом примере используется класс ZipFile из пакета java.util.zip и классы FileInputStream, FileOutputStream, ZipEntry, ZipOutputStream из пакета java.io. Эти классы предоставляют функциональность для работы с файлами ZIP. Функция FileInputStream используется для создания потока ввода для файла. Функция FileOutputStream используется для создания потока вывода для файла ZIP. Функция ZipOutputStream используется для записи данных в файл ZIP. Функция ZipEntry используется для представления файла в архиве ZIP.
