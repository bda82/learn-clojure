(ns my-file-search
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn search-files [dir pattern]
  (filter #(re-find pattern (slurp %))
          (file-seq (io/file dir))))

(defn -main [& args]
  (let [dir (first args)
        pattern (second args)]
    (when (and dir pattern)
      (println "Searching for" pattern "in" dir)
      (println (str/join "\n" (search-files dir pattern)))
      (println "Done."))
    (println "Usage: lein run <directory> <pattern>")))

;; search-files - это функция, которая принимает каталог и шаблон поиска в виде регулярного выражения. Она использует функцию file-seq из библиотеки clojure.java.io для получения последовательности файлов в каталоге. Затем, она фильтрует эту последовательность с помощью функции filter, которая применяет re-find к содержимому каждого файла.
;; -main - это главная функция, которая принимает два аргумента - каталог и шаблон поиска. Она вызывает функцию search-files и выводит результаты в консоль.

;; Чтобы использовать эту функцию, выполните следующую команду в командной строке:
lein run <directory> <pattern>

;; например (Функция выведет в консоль имена файлов, в которых найдено совпадение с шаблоном поиска)
lein run /path/to/directory "my search pattern"