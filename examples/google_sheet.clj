;; Вот пример функции на Clojure, которая заполняет данные в Google Sheet с помощью библиотеки clj-sheets:

(ns my-google-sheet
  (:require [clj-sheets.core :as sheets]))

(defn write-to-google-sheet []
  (let [spreadsheet-id "your-spreadsheet-id"
        range "Sheet1!A1:B2"
        values '[[1 "John"] [2 "Jane"]]
        credentials-path "path/to/credentials.json"]
    (sheets/write-values spreadsheet-id range values
                       {:service-account-file credentials-path})))

(defn -main []
  (write-to-google-sheet))

;; write-to-google-sheet - функция, которая заполняет данные в ячейки A1 и B2 листа Sheet1 в Google Sheet.
;; spreadsheet-id - идентификатор таблицы в формате your-spreadsheet-id.
;; range - диапазон ячеек в формате Sheet1!A1:B2.
;; values - список списков, каждый из которых представляет строку в таблице.
;; credentials-path - путь к файлу с учетными данными для доступа к таблице.
;; sheets/write-values - функция из библиотеки clj-sheets.core, которая заполняет данные в ячейки таблицы.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-google-sheet/-main)

;; Важно отметить, что для работы с Google Sheet вам нужно будет создать учетные данные для доступа к таблице и сохранить их в файл. Затем, вы должны указать путь к этому файлу в параметре credentials-path.
;; Важно также отметить, что библиотека clj-sheets использует библиотеку clj-google-auth для авторизации и доступа к API Google Sheets. Чтобы использовать clj-sheets, вам нужно будет добавить зависимость на clj-google-auth в файл project.clj:
[clj-google-auth "0.3.1"]

;; После этого, вы должны выполнить команду lein deps для установки зависимостей.

;; Полную документацию и инструкции по установке библиотеки clj-sheets вы можете найти по ссылке: https://clj-sheets.readthedocs.io/en/latest/