(ns my-excel-generator
  (:import (java.io FileOutputStream)
            (org.apache.poi.ss.usermodel.CellStyle)
            (org.apache.poi.ss.usermodel.Font)
            (org.apache.poi.ss.usermodel.Row)
            (org.apache.poi.ss.usermodel.Sheet)
            (org.apache.poi.ss.usermodel.Workbook)
            (org.apache.poi.xssf.usermodel.XSSFWorkbook)))

(defn create-excel [file-path]
  (let [output-stream (FileOutputStream. (File. file-path))
        workbook (XSSFWorkbook.)]
    (let [sheet (.createSheet workbook "Sheet1")
          style (.createCellStyle workbook)
          font (.createFont workbook)
          row (.createRow sheet 0)
          cell (.createCell row 0)]
      (.setFont font "Arial" 12)
      (.setFontHeightInPoints font 12)
      (.setBold font true)
      (.setCellStyle cell style)
      (.setCellValue cell "Hello, Excel!")
      (.write workbook output-stream)
      (.close output-stream)
      (.close workbook)))

(defn -main []
  (create-excel "path/to/excel/file.xlsx"))

;; create-excel - функция, которая создает Excel-файл.
;; file-path - путь к Excel-файлу.
;; output-stream - поток вывода для Excel-файла.
;; workbook - объект Workbook, который представляет Excel-файл.
;; sheet - объект Sheet, который представляет лист в Excel-файле.
;; style - объект CellStyle, который используется для задания стиля ячейки.
;; font - объект Font, который используется для задания шрифта ячейки.
;; row - объект Row, который представляет строку в Excel-файле.
;; cell - объект Cell, который представляет ячейку в Excel-файле.
;; -main - главная функция, которая вызывает create-excel и создает Excel-файл.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-excel-generator/-main)

;; Важно отметить, что в этом примере используется библиотека POI из Apache. Эта библиотека предоставляет функциональность для работы с Excel-файлами. Функция FileOutputStream используется для создания потока вывода для Excel-файла. Функция XSSFWorkbook используется для создания объекта, представляющего Excel-файл. Функция Sheet используется для создания объекта, представляющего лист в Excel-файле. Функция CellStyle используется для задания стиля ячейки. Функция Font используется для задания шрифта ячейки. Функция Row используется для создания объекта, представляющего строку в Excel-файле. Функция Cell используется для создания объекта, представляющего ячейку в Excel-файле.
;; Важно также отметить, что перед использованием библиотеки POI вам нужно буде
