(ns my-os-process-list
  (:import [java.lang ProcessBuilder]))

(defn get-process-list []
  (let [cmd ["ps", "-ef"]
        pb (ProcessBuilder. cmd)]
    (.inheritIO pb)
    (.start pb)
    (let [output (.getInputStream pb)]
      (clojure.string/split (slurp output) #"\n"))))

(defn -main []
  (println "List of processes:")
  (println (get-process-list))
  (println "Done."))

;; get-process-list - функция, которая запускает команду ps -ef и возвращает список строк с выводом команды.
;; -main - главная функция, которая вызывает get-process-list и выводит результаты в консоль.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-os-process-list/-main)

;; Вы увидите список процессов в консоли. Обратите внимание, что в этом примере используется команда ps -ef для Linux-систем. Если вы используете другую ОС, вам нужно будет заменить команду на соответствующую для вашей ОС. Например, для Windows можно использовать команду tasklist.
;; Важно отметить, что в этом примере используется класс ProcessBuilder из пакета java.lang. Этот класс используется для запуска внешних процессов и получения их вывода. Функция inheritIO используется для перенаправления ввода-вывода процесса в поток вывода программы. Функция slurp используется для чтения вывода процесса в строку.

