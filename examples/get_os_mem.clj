(ns my-system-info
  (:import (java.lang Runtime)))

(defn get-total-memory []
  (let [runtime (Runtime/getRuntime)]
    (.freeMemory runtime)
    (.totalMemory runtime)))

(defn get-disk-space [path]
  (let [fs (java.io.File. path)]
    (.getTotalSpace fs)))

(defn -main []
  (println "Total memory: " (/ (get-total-memory) (1024d * 1024d) "MB")
  (println "Disk space on C: " (/ (get-disk-space "C:") (1024d * 1024d * 1024d) "GB")))

;; get-total-memory - функция, которая получает размер оперативной памяти с помощью класса Runtime из пакета java.lang.
;; get-disk-space - функция, которая получает размер дискового пространства с помощью класса File из пакета java.io.
;; -main - главная функция, которая вызывает get-total-memory и get-disk-space и выводит результаты в консоль.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-system-info/-main)

;; Вы увидите размер оперативной памяти и дискового пространства в консоли.
;; Обратите внимание, что функция get-disk-space принимает путь к диску в качестве аргумента. В этом примере используется путь "C:". Если вы используете другую ОС, вам нужно будет заменить путь на соответствующий для вашей ОС.
;; Важно отметить, что в этом примере используются классы Runtime и File из пакетов java.lang и java.io соответственно. Эти классы предоставляют функциональность для работы с оперативной памятью и файловой системой соответственно.
