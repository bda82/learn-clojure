(ns my-web-server
  (:require [ring.adapter.jetty :as jetty]
            [ring.middleware.cors :as cors]
            [ring.middleware.defaults :as defaults]
            [ring.util.response :as response]
            [ring.util.html :as html]
            [compojure.core :refer :all]
            [compojure.route :as route]))

(defn hello-page []
  (html [:html
          [:head
           [:title "Hello Page"]]
           [:body
            [:h1 "Hello, World!"]
            [:p "Welcome to my web server!"]]))

(defroutes app-routes
  (GET "/" [] (hello-page))
  (route/not-found "Not Found"))

(def app
  (-> app-routes
      (cors/wrap-cors)
      (defaults/wrap-defaults)))

(defn -main []
  (jetty/run-jetty app {:port 3000}))

;; hello-page - функция, которая формирует HTML-страницу.
;; app-routes - список маршрутов, который включает в себя обработку GET-запроса на корень сайта и возвращает HTML-страницу.
;; app - приложение, которое создается путем объединения маршрутов и middleware.
;; cors/wrap-cors - middleware, которое добавляет поддержку CORS к приложению.
;; defaults/wrap-defaults - middleware, которое добавляет стандартные middleware к приложению.
;; -main - главная функция, которая запускает веб-сервер на порту 3000.

;; Чтобы запустить эту функцию, выполните функцию -main:
(my-web-server/-main)

;; Вы увидите HTML-страницу в браузере по адресу http://localhost:3000.

;; Важно отметить, что в этом примере используется фреймворк compojure для определения маршрутов и фреймворк ring для создания веб-сервера. Функция html используется для формирования HTML-страницы. Функция jetty/run-jetty используется для запуска веб-сервера.
;; Важно также отметить, что перед использованием фреймворка compojure и ring вам нужно будет добавить зависимости на них в файл project.clj:
[ring "1.9.0"]
[compojure "1.6.1"]

;; После этого, вы должны выполнить команду lein deps для установки зависимостей.
;; Полную документацию и инструкции по установке фреймворков compojure и ring вы можете найти по ссылкам:
;; https://github.com/weavejester/compojure
;; https://github.com/ring-clojure/ring