# Записи (records)

Записи в Clojure использованы для реализации некоторых возможностей объектно-ориентированного стиля программирования (не очень ясно, почему принято такое название). Для объявления записи служит директива defrecord.

(defrecord Person [name age pursuit]) → user.Person

Мы создали запись под названием Person. Фактически при этом создаётся класс Java под этим именем, поэтому и название начинается с заглавной буквы, а если имя составное, то его рекомендуют записывать в CamelCase стиле (хотя это не обязательно). Аргументы name age pursuit можно считать полями класса. Запись является представителем нового типа данных, то-есть у нас теперь есть тип данных Person, который можно использовать на равных правах с базовыми типами. Класс предоставляет в наше распоряжение конструктор, позволяющий создавать экземпляры записи и передавать полям конкретные значения. Применяется следующий синтаксис:

(def p (Person. "Anna" 21 "teacher"))

Таким образом мы создали экземпляр записи с именем p (обращаю внимание на необходимость добавлять точку к имени записи). Есть и вторая форма вместо точки:

(def p (->Person "Anna" 21 "teacher"))

Проверим с помощью директивы class тип (или можно применять директиву type):

(class Person) → java.lang.Class — действительно класс Java

(class p) → user.Person - переменная p имеет тип Person (выводится полное имя, то-есть с указанием пространства имён user). 

Посмотрим на содержимое переменной p:

p → #user.Person{:name "Anna", :age 21, :pursuit "teacher"}

Таким образом, запись имеет вид хеша, при этом имена полей преобразованы в ключевые слова. Доступ к значениям полей возможен по ключам, как обычно:
(:name p) → "Anna"
(:age p) → 21

Однако, вторая форма (p :name) для записей не допустима. Зато есть ещё и такая форма для извлечения значений:

(.name p) → "Anna"

Можно создавать сколько угодно экземпляров записи Person: 

(def p1 (Person. "Marta" 25 "engineer"))
(def p2 (Person. "Peter" 30 "doctor"))

и делать их членами коллекций:

(def v [p p1 p2])

С такими коллекциями можно работать, как с обычными, например применим к вектору v функцию map:

(map :name v) → ("Anna" "Marta" "Peter")

Здесь из каждого элемента вектора v извлекается значение по ключу :name и результат возвращается в виде списка.

Поскольку запись на самом деле представлена хэшем, к ним применим весь набор функций: assoc, keys, get, seq, conj, into и другие.

(assoc p :adress "Moscow") → #user.Person{:name "Anna", :age 21, :pursuit "teacher", :adress "Moscow"}

Здесь мы в экземпляре p записи Person добавили новое поле adress, инициированное значением "Moscow".

(keys p) → (:name :age :pursuit)

Однако, при этом поля экземпляра p не изменились (как и всё в Clojure, записи immutable), при добавлении поля создаётся новый экземпляр:

(def p3 (assoc p :adress "Moscow"))
p3 → #user.Person{:name "Anna", :age 21, :pursuit "teacher", :adress "Moscow"}

Изменять поля записи и передавать им значения можно ещё и в такой нотации:

(map->Person {:name "Bob" :age 38 :pursuit "engineer" :adress "Vologda"}) → #user.Person{:name "Bob", :age 38, :pursuit "engineer", :adress "Vologda"}

Для того, чтобы посмотреть на примерах другие возможности работы с записями и не писать громоздких выражений, объявим запись с более короткими именами полей:

(defrecord Point [x y])

Для передачи полям значений можно применять функцию apply со стрелкой:

(apply ->Point [5 6]) → #user.Point{:x 5, :y 6}

Комбинируя функции map, partial, apply можно сразу создавать коллекцию из экземпляров записей:

(def col (map (partial apply ->Point) [[5 6] [7 8] [9 10]])) → #'user/col col → (#user.Point{:x 5, :y 6} #user.Point    
    {:x 7, :y 8} #user.Point{:x 9, :y 10})

Здесь col – список с элементами — экземплярами записей Point.

(first col) → #user.Point{:x 5, :y 6} – извлекли первый элемент списка 
(map map->Point [{:x 1 :y 2} {:x 5 :y 6 :z 44}]) → (#user.Point{:x 1, :y 2} #user.Point{:x 5, :y 6, :z 44})

теперь передали значения и ввели новое поле для двух экземпляров Point.
