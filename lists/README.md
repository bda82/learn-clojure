## Списки

Извлечение по индексу (нумерация начинается с 0). Этот способ показывает, что вектор — это функция своих индексов.
```clojure
(v 1) → "foo"
```

conj - добавляет элемент в коллекцию.
```clojure
(conj [1 2 3] 4) ; => [1 2 3 4]
```

first - возвращает первый элемент коллекции.
```clojure
(first [1 2 3]) ; => 1
```

second - извлечение второго элемента коллакции.
```clojure
(second v) → "foo" - соответственно, второй элемент
```

rest - возвращает все элементы коллекции, кроме первого.
```clojure
(rest [1 2 3]) ; => (2 3)
```

last - последний элемент, у нас это вложенный вектор
```clojure
(last v) → [5 12]
```

nth - возвращает n-й элемент коллекции.
```clojure
(nth [1 2 3] 1) ; => 2
```

count - возвращает количество элементов в коллекции.
```clojure
(count [1 2 3]) ; => 3
```

map - применяет функцию ко всем элементам коллекции.
```clojure
(map inc [1 2 3]) ; => (2 3 4)
```

reduce - сворачивает коллекцию с использованием заданной функции.
```clojure
(reduce + [1 2 3]) ; => 6
```

filter - возвращает коллекцию, содержащую только элементы, для которых предикат возвращает истину.
```clojure
(filter odd? [1 2 3]) ; => (1 3)
```

remove - возвращает коллекцию, содержащую только элементы, для которых предикат возвращает ложь.
```clojure
(remove odd? [1 2 3]) ; => (2)
```

sort - возвращает отсортированную коллекцию.
```clojure
(sort [3 1 2]) ; => (1 2 3)
```

## Вектора
conj - добавляет элемент в конец вектора.
```clojure
(conj [1 2 3] 4) ; => [1 2 3 4]
```

assoc - заменяет элемент в векторе на указанной позиции.
```clojure
(assoc [1 2 3] 1 20) ; => [1 20 3]
```

pop - удаляет последний элемент вектора.
```clojure
(pop [1 2 3]) ; => [1 2]
```

subvec - возвращает подвектор вектора от начального индекса до конечного.
```clojure
(subvec [1 2 3 4 5] 1 3) ; => [2 3]
```

vec - преобразует коллекцию в вектор.
```clojure
(vec '(1 2 3)) ; => [1 2 3]
```

vector - создает новый вектор с указанными элементами.
```clojure
(vector 1 2 3) ; => [1 2 3]
```

vector-of - создает новый вектор определенного типа.
```clojure
(vector-of :int 1 2 3) ; => [1 2 3]
```

into - добавляет все элементы одной коллекции в другую.
```clojure
(into [1 2] [3 4]) ; => [1 2 3 4]
```

get - возвращает элемент вектора на указанной позиции.
```clojure
(get [1 2 3] 1) ; => 2
```

peek - возвращает последний элемент вектора.
```clojure
(peek [1 2 3]) ; => 3
```

Функция keep: Эта функция принимает функцию и коллекцию, и возвращает новую коллекцию, содержащую результаты вызова функции для каждого элемента, если результат не nil.
```clojure
(keep #(if (odd? %) % nil) [1 2 3 4 5]) ; => (1 3 5)
```

Покажем теперь несколько примеров использования этих методов в операциях над элементами вектора.

```clojure
(+ (first v) (v 2)) → 141.2 - сумма первого и третьего элементов
(+ (first v) (first (last v))) → 47 - сумма первого элемента и первого элемента вложенного вектора
```

В следующем примере мы олучили сумму первого элемента с элементами вложенного вектора. Поскольку второй и третий элементы не использованы, для них можно не вводить оригинальное имя, а использовать знаки подчёркивания. Обычно этот знак означает «какой угодно» или «любой» и называется групповым символом или placeholder.

```clojure
(let [[x _ _ [y z]] v] (+ x y z)) → 59
```

Можно также использовать знак &, указывающий на множество элементов.

```clojure
(let [[x & r] v] r) → ("foo" 99.2 [5 12])
```

Здесь форму [x & r] надо понимать, как первый элемент и все остальные, которыми инициируется переменная r. Результат получен в форме списка.

При использовании списков иногда приходится принудительно изменять порядок следования элементов (выполнять реверс). Для примера создадим функцию mp – аналог встроенной функции map.

```clojure
(defn mp [f c] (loop [co c, ac nil]
(if (empty? co)(reverse ac)(recur (next co)(cons (f (first co)) ac))))) 
```

Функция mp получает два аргумента: функцию f (она выполняет заданные действия над элементами списка) и коллекцию — список c.

Функция pos принимает целое число — индекс e и коллекцию c. 
В этом примере использована не встречавшаяся ранее функция for, которая напоминает классический оператор цикла for, но имеет больше возможностей. Эта функция инициирует переменные i, v соответствующими значениями из пар, составляющих коллекцию — результат работы функции f. При совпадении переменной v с заданным индексом e функция pos возвращает первый элемент пары.
Посмотрим на результаты использования функции pos:

```clojure
(pos 3 [:a 1 :b 2 :c 3 :d 4]) → (5) - напоминаю, что в этом случае коллекция, возвращаемая функцией f имеет вид:
([0 :a] [1 1] [2 :b] [3 2] [4 :c] [5 3] [6 :d] [7 4])
(pos 3 {:a 1, :b 2, :c 3, :d 4}) → (:c)
(pos 3 [:a 3 :b 3 :c 3 :d 4]) → (1 3 5)
(pos 3 {:a 3, :b 3, :c 3, :d 4}) → (:a :b :c)
```

## Как можно применить функцию map для работы с векторами

Функция map в Clojure применяет указанную функцию ко всем элементам коллекции и возвращает новую коллекцию с результатами. Это работает также и с векторами. Например, если вы хотите увеличить каждый элемент вектора на 1, вы можете сделать это следующим образом:
```clojure
(map inc [1 2 3]) ; => (2 3 4)
```

Обратите внимание, что map возвращает ленивую последовательность, а не вектор. Если вам нужен вектор, вы можете преобразовать результат в вектор с помощью функции vec:
```clojure
(vec (map inc [1 2 3])) ; => [2 3 4]
```

Или вы можете использовать функцию mapv, которая возвращает вектор:
```clojure
(mapv inc [1 2 3]) ; => [2 3 4]
```

Функция filterv: Это вариант функции filter, который возвращает вектор вместо последовательности.
```clojure
(filterv odd? [1 2 3 4 5]) ; => [1 3 5]
```

В Clojure, векторы и списки являются разными типами коллекций и имеют различные характеристики и свойства.

Время доступа к элементам: Векторы в Clojure предоставляют быстрый произвольный доступ к элементам, т.е. доступ к элементу по индексу выполняется за постоянное время. Списки, с другой стороны, являются связными списками, и доступ к элементам выполняется за линейное время.

Добавление элементов: Векторы оптимизированы для добавления элементов в конец, в то время как списки оптимизированы для добавления элементов в начало.

Использование в коде: Векторы часто используются для представления последовательностей значений или структурированных данных, в то время как списки часто используются для представления кода в макросах и функциях, потому что они автоматически интерпретируются как вызовы функций или макросов.

Вот примеры использования векторов и списков:

```clojure
(def my-vector [1 2 3]) ; вектор
(def my-list '(1 2 3)) ; список
```

В обоих случаях вы можете использовать функции first, rest, conj и т.д., но результаты и производительность могут отличаться.
