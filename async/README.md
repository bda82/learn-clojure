core.async - это библиотека Clojure для асинхронного программирования и обработки событий. Она предоставляет примитивы для асинхронного потока управления и обмена данными.

Вот пример использования core.async для создания двух асинхронных процессов, которые обмениваются сообщениями через канал:

```clojure
(ns my-namespace.core
    (:require [clojure.core.async :as async :refer [<! >! go chan close!]]))

(defn producer [out]
    (go
        (loop [i 0]
            (when (< i 10)
                (>! out i)
                (recur (inc i))))
        (close! out)))

(defn consumer [in]
    (go
        (loop []
            (when-let [v (<! in)]
                (println "Received:" v)
                (recur)))))

(defn -main []
    (let [c (chan)]
        (producer c)
        (consumer c)))
```

В этом примере функция producer генерирует числа от 0 до 9 и отправляет их в канал. Функция consumer читает числа из канала и выводит их. Обе функции запускаются асинхронно с помощью макроса go.

Обратите внимание, что операции чтения и записи в канал (<! и >!) должны выполняться внутри макроса go, который обеспечивает асинхронное выполнение кода.

Вот пример асинхронного запроса к API с использованием библиотеки http-kit и core.async в Clojure:

```clojure
(ns my-namespace.core
    (:require [org.httpkit.client :as http]
                        [clojure.core.async :as async :refer [<! >! go chan close!]]))

(defn async-get [url]
    (let [response-chan (chan)]
        (http/get url
                            {:as :json
                             :timeout 10000
                             :callback (fn [response]
                                                     (go (>! response-chan response)))})
        response-chan))

(defn -main []
    (let [response-chan (async-get "http://example.com/api/data")]
        (go
            (let [response (<! response-chan)]
                (println "Status:" (:status response))
                (println "Body:" (:body response))))))
```

В этом примере функция async-get выполняет асинхронный GET-запрос к указанному URL и возвращает канал, в который будет отправлен ответ. Функция -main выполняет запрос и выводит результат, когда он становится доступен.

Обратите внимание, что http/get возвращает сразу, а ответ от сервера обрабатывается в колбэке, который отправляет ответ в канал. Это позволяет функции -main продолжить выполнение и обработать ответ, когда он станет доступен.