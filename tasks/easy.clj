;; Алгебраическая сумма ряда чисел

;; Геометрическая прогрессия

;; Алгебраическая сумма ряда степеней вида 1^k + 2^k + 3^k + ... + N^k

;; Запрограммировать генератор псевдослучайных чисел, используя конгруэнтный метод формирования псевдослучайных чисел в интервале [0;1)].
;; Yn+1 = (SUM[i=0;j] a * Yn + c) mod m
;; Xn+1 = Yn+1 / m
;; где, a0, a1, . . . , aj — множители
;; µ — значение инкремента
;; y1, y2, . . . , yn — получаемые случайные числа
;; x1, x2, . . . , xn — случайные числа на интервале [0;1)

;; Запрограммировать генератор псевдослучайных чисел, используя метод середины квадрата.
;; Xn+1 = (Xn^2) / 10^d
;; где, Xn — получаемые случайные числа
;; Xn+1 — случайные числа на интервале [0;1)
;; d — количество цифр в Xn

;; По заданному вещественному x, вычислить корень кубический из x по следующей итерационной формуле:
;; Yi+1= 0.5 ( Yi + 3 * X / ( 2 Yi2 + X / Yi )).

;; Найти простые числа, используя Решето Эратосфена
;; С клавиатуры вводится число N (типа int). 
;; Используя алгоритм «Решето Эратосфена», необходимо найти все простые числа (т.е. делящиеся только на себя и на единицу) в интервале [0;N].

;; Вычислить произведение матрицы на вектор

;; Заполнить двумерный массив зигзагом по диагонали

;; Вывести содержимое матрицы в виде Z

;; Переписать все элементы двумерного массива в одномерный

;; Найти след матрицы, след матрицы — сумма элементов главной диагонали. Размер матрицы вводит пользователь, матрицу заполнять случайными числами.

;; Квадратная матрица, главная и побочные диагонали
;; Дана квадратная матрица размером nxn. 
;; Найти минимальный элемент среди элементов, расположенных ниже главной диагонали, 
;; найти максимальный элемент, среди элементов расположенных выше побочной диагонали. 
;; Найденные минимальный и максимальный элементы поменять местами и вывести их индексы.

;; Необходимо разработать программу, которая предоставляет удобный функционал для работы с разреженной матрицей. 
;; Разреженная матрица — матрица с большим количеством нулевых элементов. 
;; Требуется написать следующие функции:
;; - Ввода матрицы;
;; - Печати матрицы;
;; - Суммирования двух матриц;
;; - Умножения двух матриц.

;; Составить программу, которая по введенной месячной зарплате, вычисляет подоходный налог.

;; Составить программу роста вклада в банке. Вклад составляет X рублей. Годовой процент составляет P процентов. Вкладывается на Y лет без пополнения. 
;; Составить программу, которая по введенным числам X, P, Y вычисляет сумму вклада через Y лет.

;; То же самое, но с пополнением вклада каждый год на Z рублей.

